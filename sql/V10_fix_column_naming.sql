alter table blueshell.users
    change photoConsent photo_consent bool;
alter table blueshell.users
    change startStudyYear start_study_year int;
